package code.niki.LeadTracker;

import java.net.URI;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import code.niki.DynamoObjectMappers.DynamoPrimaryUserDetail;
import code.niki.DynamoObjectMappers.DynamoSessionDetail;
import code.niki.HelperLibs.DynamoAccess;



@Path("TrackLead")
public class TrackLead {
	private static final Logger logger = LogManager.getLogger(TrackLead.class);
	
	@Path("trackNredirect")
	@GET
	public Response setTrackLead(@QueryParam("CardName") String CardName ,@QueryParam("ButtonName") String ButtonName ,
									@QueryParam("sessionId") String sessionId, @QueryParam("sdkMerchant") String sdkMerchant  ) {
		Response response = null;
		DynamoSessionDetail sessionDetail = null;
		DynamoPrimaryUserDetail userDetail = null;
		
		try {
			sessionDetail = new DynamoAccess().getSessionBySessionId(sessionId);
			userDetail = new DynamoAccess().getPrimaryUserByUserId(sessionDetail.getUserId());
			logger.info("Fetched sessiondetails with sessionId: "+sessionDetail.getSessionId()+ " and merchantId: " + sessionDetail.getChatBox());
			
			String web_url ="";
			String Phone = userDetail.getPhoneNumber();
			String Name = userDetail.getName();
			String Email =userDetail.getUserEmail();
			long Timestamp;
			
			try {
				//SELECT web_url from Reference_table where Button_name = ButtonName annd Card_name = CardName;
				//INSERT into user_table values ;
				
			}
			catch(Exception e){
				logger.error("error in accessing database" , e);
			}
			logger.info("URI " + URI.create(web_url));
			response = Response.seeOther(URI.create(web_url)).build();
		}
		catch(Exception e)
		{
			logger.error("error in redirecting ", e);
			response = Response.status(500).build();
		}
		return response;
	}
	
	
}
