package code.niki.LeadTracker;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.threadpool.GrizzlyExecutorService;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;


public class Main {
	
	// Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://0.0.0.0:5025/";
	private static final Logger logger = LogManager.getLogger(Main.class);
	
	
	 /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.example package
        final ResourceConfig rc = new ResourceConfig();
       // rc.register(com.redirects.CORSResponseFilter.class);
        final Map<String, Object> initParams = new HashMap<String, Object>();
        initParams.put(ServerProperties.WADL_FEATURE_DISABLE, "true");
        rc.addProperties(initParams);
        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
		//reconfigure the thread pool
		NetworkListener listener = server.getListeners().iterator().next();
		ThreadPoolConfig config2 = listener.getTransport().getWorkerThreadPoolConfig();
		config2.setCorePoolSize(10);
		config2.setMaxPoolSize(300);
		GrizzlyExecutorService threadPool = (GrizzlyExecutorService) listener.getTransport().getWorkerThreadPool();
		threadPool.reconfigure(config2);
		return server;
    }

	public static void main (String[] args)
	{
		final HttpServer server = startServer();
	}
}
